Usage
============

On first boot, you will be prompted to download the AdBlock filter lists, please confirm and download the lists.

After this step, you are ready to go.



## Gestures
-----------
This might differ depending which OS/Windowmanager you are using.

*Swipe from left* => see Hamburger Menu

*Swipe from right* => see Kebab Menu


## Hamburger Menu
-----------------

![adblocker](img/menubar_hamburger.png)

![adblocker](img/menu_hamburger.png)




## Kebab Menu
-------------

![adblocker](img/menubar_kebab.png)

![adblocker](img/menu_kebab.png)





## Addressbar
-------------

Sometimes the addressbar hides important webcontent.

To hide the addressbar select in Kebab menu *"Hide Navigation Bar"*.
