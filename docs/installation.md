Installation
============

Manjaro:



    sudo pacman -S angelfish

or open the "Discover" app and install it from there.

&nbsp;

Mobian:


get a prebuild arm packed here:

	https://jbb.ghsq.ga/debpm/pool/main/p/plasma-angelfish/

or install from the Software store (this contains the adblocker) after enabling:

	apt install gnome-software-plugin-flatpak
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo


&nbsp;

Postmarket/Alpine based:


    sudo apk add plasma-angelfish

Note that the PostmarketOS build of Angelfish has no AdBlock 
functionality.


&nbsp;




Uninstall
=========


Manjaro:



    sudo pacman -R angelfish



    
