AdBlocker
=========


Angelfish has one of the best AdBlocker available.


On first start Angelfish will ask you to download the filter lists and then its important to make sure "Enable Adblock" is highlighted in the menu to be active:



![adblocker](img/adblocker.png)

