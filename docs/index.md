![angelfish](img/angelfish.png)
# Angelfish
Pinephone Usermanual


Angelfish is an experimental webbrowser designed to be used on small mobile devices and ARM based hardware.

This usermanual is explicit written for the usage on a [Pinephone](https://www.pine64.org/pinephone).


See the [project repo](https://invent.kde.org/plasma-mobile/angelfish) for more information.

&nbsp;


You are welcome to contribute [to this doc](https://invent.kde.org/magdesign/angelfish-pinephone-documentation).

&nbsp;

# FAQ

- **Adblock does not seem to work** => Make sure ```Enable Adblock``` is highlighted in the hamburger menu.
<br/>

- **Address bar is hiding some content** => In kebab menu, select  ```Hide navigation bar```.
<br/>

- **Delete cookies** => <br/>
    on Manjaro:  ```rm ~/.local/share/KDE/angelfish/QtWebEngine/DefaultProfile/Cookies``` <br/>
    on Mobian:  ```rm -rf ~/.var/app/org.kde.angelfish/data/KDE/angelfish/QtWebEngine/DefaultProfile/Cookies```<br/>

- **Video playback stutters** => there is currently nothing to do, since we need  hardware accelerated video playback on the Pinephone. 
