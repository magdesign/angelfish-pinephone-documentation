# Welcome

This is becoming the help page for Angelfish on Pinephone.
Its in the early stages since I am figuring out how to create this, any help welcome.

Its written with [mkDocs](https://www.mkdocs.org/).

The rendered page will  be available on [here](https://magdesign.github.io/Angelfish_Pinephone_Documentation/html/index.html) until someone helps me to figure out on how to render html pages on gitlab.

After editing the pages in the docs folder, go to root and type:  ```mkdocs build ```
to create the html site.
